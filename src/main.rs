extern crate syscall;

use syscall::scheme::SchemeMut;
use syscall::error::{Error, Result, ENOENT, EBADF, EINVAL};

use std::cmp::min;

struct EjiekScheme {
    vec: Vec<u8>,
}

impl EjiekScheme {
    fn new() -> EjiekScheme {
        EjiekScheme {
            vec: Vec::new(),
        }
    }
}

impl SchemeMut for EjiekScheme {
    fn open(&mut self, path: &[u8], _flags: usize, _uid: u32, _gid: u32) -> Result<usize> {
        self.vec.extend_from_slice(path);
        Ok(path.len())
    }

	fn read(&mut self, _id: usize, buf: &mut [u8]) -> Result<usize> {
        let res = min(buf.len(), self.vec.len());

        for b in buf {
            *b = if let Some(x) = self.vec.pop() {
                x
            } else {
                break;
            }
        }

        Ok(res)
    }

   fn write(&mut self, _id: usize, buf: &[u8]) -> Result<usize> {
        let mut reverse = Vec::new();
        for &i in buf {
            reverse.push(i);
        }

		for symbol in reverse.iter().rev() {
	        self.vec.push(*symbol);
		}

        Ok(buf.len())
    }
}


fn main() {
    use syscall::data::Packet;
    use std::fs::File;
    use std::io::{Read, Write};
    use std::mem::size_of;

    let mut scheme = EjiekScheme::new();
    // Create the handler
    let mut socket = File::create(":ejiek").unwrap();
    loop {
        let mut packet = Packet::default();
		println!("{:?}", packet);	
        while socket.read(&mut packet).unwrap() == size_of::<Packet>() {
            scheme.handle(&mut packet);
            socket.write(&packet).unwrap();
        }
    }
}
